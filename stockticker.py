import ystockquote
import json

stocks =  ystockquote.get_historical_prices('SAI', '20130101', '20130516')

#skip first row
iterstocks = iter(stocks)
next(iterstocks)

stock_val = []
for stock in iterstocks:
    stock_val.append([stock[0], float(stock[6])])

stock_val_json = json.dumps(stock_val)

data = open('stockdata.json', 'w')

data.write(stock_val_json)

data.close()

